// здесь фукнция посредник
import { Authorization, AdminSeal, Expressions } from './services/index';

const ADMIN = {
  email: 'admin@technaxis.com',
  password: 'password',
};

const apiProvider = () => ({
  authorization: () => new Authorization(), 
  adminSeal: () => new AdminSeal(), 
  adminExpresiions: () => new Expressions(),
});

export { apiProvider, ADMIN };