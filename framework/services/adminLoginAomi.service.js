import supertest from 'supertest';
import { urls } from '../config';
import path from 'path';

const Authorization = function Authorization() { 

  this.post = async function (params) { // предаем параметры которые будем отправлять
    reporter.startStep("Вызываем post '/v1/client/login'"); // для аллюра 
    
    const r = await supertest(urls.aomi)
      .post('/v1/client/login') //метод POST 
      .send(params); // отправка парамертов 

    reporter.endStep(); 
    //console.log(r);
    return r;
  };


  this.uploadImageFile = async function (token, fileName) { 
    reporter.startStep("Вызываем post '/v1//v1/files/upload'"); // для аллюра 
    
    const r = await supertest(urls.aomi)
      .post('/v1/files/upload') //метод POST 
      .set('Authorization', `Bearer ${token}`)
      .query({ module: 'SEALS' }) // перелаем query парметр module
      .attach('file', path.resolve(__dirname, './files/'+ fileName)); // product_33da0a83.jpg
    

    reporter.endStep(); 
    
    return r;
  };




  this.uploadAudioFile = async function (token, fileName) { 
    //reporter.startStep("Вызываем post '/v1/files/upload'"); // для аллюра 
    
    const r = await supertest(urls.aomi)
      .post('/v1/files/upload') //метод POST 
      .set('Authorization', `Bearer ${token}`)
      .query({ module: 'EXPRESSION' }) // перелаем query парметр module
      .attach('file', path.resolve(__dirname, './files/'+ fileName)); // .mp3
    
    

    //reporter.endStep(); 
    //console.log('r=', r.status);
    return r;
  };


  

};

export { Authorization }; 
