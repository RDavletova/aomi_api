import supertest from 'supertest';
import { urls } from '../config';

const AdminSeal = function AdminSeal() { 

    this.createSeal = async function (token, params) { 
        reporter.startStep("Вызываем post '/v1/admin/seals'");

        const r = await supertest(urls.aomi)
            .post('/v1/admin/seals') // метод post
            .set('Authorization', `Bearer ${token}`)
            .send(params); // передаем параметры

        reporter.endStep();
        return r;
    };


    this.getSeal = async function (token, sealId) {
        reporter.startStep("Вызываем  get '/v1/admin/seals/' + `${sealId}`");

        const r = await supertest(urls.aomi)
            .get('/v1/admin/seals/' + `${sealId}`) //метод GET
            .set('Authorization', `Bearer ${token}`)
            .send();

        //console.log(r);
        reporter.endStep();
        return r;
    };


    this.updateSeal = async function (token, sealId, form_data) {
        reporter.startStep("Вызываем  put '/v1/admin/seals/' + `${sealId}`");

        const r = await supertest(urls.aomi)
            .put('/v1/admin/seals/' + `${sealId}`) // метод PUT
            .set('Authorization', `Bearer ${token}`)
            .send(form_data); // парметры передаем

        //console.log(r);
        reporter.endStep();
        return r;        
    };


    this.deleteSeal = async function (token, sealId) {
        reporter.startStep("Вызываем delete '/v1/admin/seals/' + `${sealId}`");

        const r = await supertest(urls.aomi)
            .delete('/v1/admin/seals/' + `${sealId}`) // метод DELETE
            .set('Authorization', `Bearer ${token}`)
            .send();

        //console.log(r);
        reporter.endStep();
        return r;
    };

}

export { AdminSeal };  


