import supertest from 'supertest';
import { urls } from '../config';
const fetch = require("node-fetch");


const Expressions = function Expressions() {

  this.createExpression = async function (token, sectionId, params) { // создание выражения
    reporter.startStep("вызываем post '/v1/admin/expressions/sections/' + `${sectionId}`)");

    const r = await supertest(urls.aomi)
      .post('/v1/admin/expressions/sections/' + `${sectionId}`) // метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    reporter.endStep();
    return r;
  };


  this.getExpression = async function (token, id) { // получение выражения
    reporter.startStep("Вызываем  get '/v1/admin/expressions/' + `${id}`");

    const r = await supertest(urls.aomi)
      .get('/v1/admin/expressions/' + `${id}`) // метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };

};


export { Expressions };