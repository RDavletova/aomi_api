module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default', [
      'jest-html-reporters',
      {
        filename: 'report.html',
        expand: true,
        pageTitle: 'Aomi_Report'
      }
    ],
    'jest-stare',
    ['./node_modules/@testomatio/reporter/lib/adapter/jest.js', { apiKey: 'bac1baef44eh' }], // ключ apiKey вставляем 


  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/aom*.spec.*'], // ['**/specs/*.spec.*']
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
  setupFilesAfterEnv: ["jest-allure/dist/setup"] // аллюр  установили

};
