import faker, { random } from 'faker';
import { apiProvider, ADMIN } from '../framework';



describe('testsuit for Seal', () => {
  test('Authorization for admin', async () => {

    const r = await apiProvider().authorization().post(ADMIN); // 

    expect(r.status).toBe(200);
    expect(r.body.result.role).toBe('ADMIN');
    expect(r.body.result.userId).toBe(1);
  });

  // test.skip('Upload ImageFiles', async () => {

  //     const rr = await apiProvider().authorization().post(ADMIN);
  //     const token = rr.body.result.authToken; // токен
  //     const fileName = 'product_33da0a83.jpg';

  //     const r = await apiProvider().authorization().uploadImageFile(token, fileName);
  //     //console.log('r.body.result.id', r.body.result.id);

  //     expect(r.status).toBe(201);
  // });


  test('Upload AudioFiles', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken; // токен
    const fileName = 'диктор 1.mp3';

    const r = await apiProvider().authorization().uploadAudioFile(token, fileName);

    expect(r.status).toBe(201);
  });




  test('create Seal', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;
    const fileName = 'product_33da0a83.jpg';

    const file = await apiProvider().authorization().uploadImageFile(token, fileName);

    const formData = { // вход данные
      hiragana: "あなた",
      imageId: file.body.result.id,

      localizations: [
        {
          languageId: 1,
          name: "Золотая Печать",
          reference: "отсылка",
          thought: "мудрая мысль у печати"
        },
        {
          languageId: 2,
          name: "Golden Seal",
          reference: "history of golden seal",
          thought: "just a wise thought just a wise thought"
        },
        {
          languageId: 3,
          name: "ゴールドシール",
          reference: "ゴールドシール",
          thought: "ゴールドシールゴー"
        }
      ]
    };

    const r = await apiProvider().adminSeal().createSeal(token, formData);

    expect(r.status).toBe(200);
  });


  test('get Seal', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const sealId = 74; // РАНДОМНОЕ ЗНАЧЕНИЕ БРАТЬ!

    const r = await apiProvider().adminSeal().getSeal(token, sealId);

    expect(r.body.result.id).toBe(sealId);

    expect(r.status).toBe(200);

  });


  test('update Seal', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const fileName = 'product_33da0a83.jpg';

    const file = await apiProvider().authorization().uploadImageFile(token, fileName);

    const formData = { // вход данные
      hiragana: "あなた",
      imageId: file.body.result.id,
      localizations: [
        {
          languageId: 1,
          name: "Золотая Печать1",
          reference: "отсылка",
          thought: "мудрая мысль у печати"
        },
        {
          languageId: 2,
          name: "Golden Seal1",
          reference: "history of golden seal",
          thought: "just a wise thought just a wise thought"
        },
        {
          languageId: 3,
          name: "ゴールドシール1",
          reference: "ゴールドシール",
          thought: "ゴールドシールゴー"
        }
      ]
    };

    const sealId = 74; // РАНДОМНОЕ ЗНАЧЕНИЕ БРАТЬ!
    const r = await apiProvider().adminSeal().updateSeal(token, sealId, formData);

    expect(r.body.result.id).toBe(sealId);
    expect(r.status).toBe(200);
  });



  test('delete Seal', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const sealId = 92; // РАНДОМНОЕ ЗНАЧЕНИЕ БРАТЬ!

    const r = await apiProvider().adminSeal().deleteSeal(token, sealId);

    //console.log(r.body.result)
    expect(r.status).toBe(204);

  });

});



describe('testsuit for Expressions', () => {

  test('create Mini-phrases', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const fileName = 'диктор 2.mp3';

    const file = await apiProvider().authorization().uploadAudioFile(token, fileName); // грузим файл

    //console.log('file=', file.status);




    const formData = { // вход данные
      examples: [
        {
          recordingId: file.body.result.id,  //416
          rows: [
            {
              hints: [
                {
                  hint: "string",
                  index: 0
                }
              ],
              row: "string"
            }
          ],
          translations: [
            {
              languageId: 1,
              translation: "ПЕревод мини-фразы на русском ПЕревод мини-фразы на русском ПЕревод фразы на русском ПЕревод фразы на русском"
            },
            {
              languageId: 2,
              translation: "translation phrase in english translation phrase in english translation phrase in english translation phrase in english"
            }
          ]
        }
      ],
      hieroglyphs: "英語のイディオム",
      hiragana: "英語のイディオム",
      localizations: [
        {
          languageId: 1,
          localization: "минифраза на русском минифраза на русском минифраза на русском"
        },
        {
          languageId: 2,
          localization: "phrase in english phrase in english phrase in english phrase"
        },
        {
          languageId: 3,
          localization: "phrase in english phrase in english phrase in english phrase english"
        }
      ],
      recordingId: file.body.result.id
    };

    const sectionId = 9; // минифраза


    const r = await apiProvider().adminExpresiions().createExpression(token, sectionId, formData);

    expect(r.status).toBe(200);
    expect(r.body.result.sectionId).toBe(sectionId);

  });


  test.skip('create Onomatope', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const fileName = 'product_33da0a83.jpg';
    const file = await apiProvider().authorization().uploadImageFile(token, fileName);

    const formData = { // вход данные   
      examples: [
        {
          recordingId: file.body.result.id,
          rows: [
            {
              hints: [
                {
                  hint: "緒に日本語を学",
                  index: 0
                },
                {
                  hint: "緒に日本語を学",
                  index: 1
                }
              ],
              row: "緒に日本語を学びましょう"
            },
            {
              hints: [
                {
                  hint: "ようこそ",
                  index: 0
                },
                {
                  hint: "ようこそ",
                  index: 1
                }
              ],
              row: "ようこそ 日本へようこそ"
            }
          ],
          translations: [
            {
              languageId: 1,
              translation: "Добро пожаловать в Японию!"
            },
            {
              languageId: 2,
              translation: "Welcom to JApen!"
            }
          ]
        }
      ],
      japanese: "日本へようこそ",
      localizations: [
        {
          languageId: 1,
          localization: "давай учить японский вместе давай учить японский вместе"
        },
        {
          languageId: 2,
          localization: "lets learn japanese together  lets learn japanese together"
        },
        {
          languageId: 3,
          localization: " 一緒に日本語を学びましょう 一緒に日本語を学びましょう"
        }
      ],
      pictureId: file.body.result.id,
      recordingId: file.body.result.id,
      subsectionId: null
    };

    const sectionId = 8; // ономатопэ

    const r = await apiProvider().adminExpresiions().createExpression(token, sectionId, formData);

    expect(r.status).toBe(200);
    expect(r.body.result.sectionId).toBe(sectionId);

  });


  test('create JapeneseSoul', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const formData = { // вход данные
      japanese: "英語のイディオム 英語のイディオム",
      localizations: [
        {
          languageId: 1,
          localization: "японская душа на русском», - слов",
          additionalDescription: " Описание японской душа на русском» - выражение"
        },
        {
          languageId: 2,
          localization: "japense soul in english",
          additionalDescription: " Description for japense soul in english"
        },
        {
          languageId: 3,
          additionalDescription: " 英語のイディオム"
        }
      ],
      subsectionId: null
    };

    const sectionId = 7; //японская душа

    const r = await apiProvider().adminExpresiions().createExpression(token, sectionId, formData);

    expect(r.status).toBe(200);
    expect(r.body.result.sectionId).toBe(sectionId);

  });



  test.skip('create Idiom', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const formData = { // вход данные
      examples: [
        {
          id: 0,
          recordingId: 421,
          rows: [
            {
              hints: [
                {
                  hint: "英語のイディオム",
                  index: 0
                },
                {
                  hint: "英語のイディオム",
                  index: 1
                }
              ],
              row: "英語のイディオム英語のイディオム"
            }
          ],
          translations: [
            {
              languageId: 1,
              translation: "Первый ПЕревод идиомы на русском ПЕревод мини-фразы на русском"
            },
            {
              languageId: 2,
              translation: "the first translation idioms in english translation idiom in english translation"
            }
          ]
        },
        {
          id: 1,
          recordingId: 422,
          rows: [
            {
              row: "英語のイディオム"
            }
          ],
          translations: [
            {
              languageId: 1,
              translation: "Второй перевод мини-фразы на русском ПЕревод мини-фразы"
            },
            {
              languageId: 2,
              translation: "tht second translation phrase in english translation"
            }
          ]
        },
        {
          id: 2,
          recordingId: 423,
          rows: [
            {
              row: "英語のイディオム"
            }
          ],
          translations: [
            {
              languageId: 1,
              translation: "третий перевод идиомы на русском ПЕревод"
            },
            {
              languageId: 2,
              translation: "the third translation idiom in english"
            }
          ]
        }
      ],
      hieroglyphs: "英語のイディオム",
      hiragana: "英語のイディオム",
      localizations: [
        {
          languageId: 1,
          localization: "идиома на русском  на русском идиома на русском",
          additionalDescription: "история на русском история на русском"
        },
        {
          languageId: 2,
          localization: "in english idiom in english idiom",
          additionalDescription: "history in enalish history in enalish"
        },
        {
          languageId: 3,
          localization: "英語のイディオム英語のイディオム",
          additionalDescription: "英語のイディオム 英語のイディオム"
        }
      ],
      recordingId: 424,
      subsectionId: null
    };

    const sectionId = 10; // идиома

    const r = await apiProvider().adminExpresiions().createExpression(token, sectionId, formData);

    expect(r.status).toBe(200);
    expect(r.body.result.sectionId).toBe(sectionId);

  });


  test('get Idiom', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const id = 36;

    const r = await apiProvider().adminExpresiions().getExpression(token, id);

    expect(r.body.result.id).toBe(id);
    expect(r.status).toBe(200);

  });


  test('get Mini-phrases', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const id = 33;

    const r = await apiProvider().adminExpresiions().getExpression(token, id);

    //console.log(r.body.result)
    expect(r.body.result.id).toBe(id);
    expect(r.status).toBe(200);

  });


  test('get JapeneseSoul', async () => {

    const rr = await apiProvider().authorization().post(ADMIN);
    const token = rr.body.result.authToken;

    const id = 7;

    const r = await apiProvider().adminExpresiions().getExpression(token, id);

    expect(r.body.result.id).toBe(id);
    expect(r.status).toBe(200);

  });


});




